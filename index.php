<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML xmlns="http://www.w3.org/1999/xhtml">
<HEAD>
<TITLE>EtreeWiki - Naming Standards</TITLE>
<META name="Language" content="en-us">
<META name="Resource-type" content="document">
<META name="Distribution" content="global">
<META name="MSSmartTagsPreventParsing" content="TRUE">
<META name="DESCRIPTION" content="etree.org Wiki">
<META name="KEYWORDS" content="etree shn music jambands jam bands download ftp Etree Wiki">
<link rel="shortcut icon" href="favicon.ico">
<LINK REL=StyleSheet HREF="wiki.css" TYPE="text/css" MEDIA=screen>
</HEAD>
<BODY>
<!-- BEGIN HEDE BANNER -->
<!-- NOTE:  I will probably be turning the top .png grahics into two seperate images rather than three... -->
<table width="100%" height="100" border="0" cellpadding="0" cellspacing="0">
 <tr>
  <td class="left" valign="top" cellpadding="0" cellspacing="0">
  <p class="hedetop">
  <FORM ACTION="/index.php?action=find" METHOD="get">
  <INPUT TYPE="hidden" VALUE="find" NAME="action">
  
  <INPUT SIZE="12" onBlur="Javascript: if(this.value == ''){this.value = 'Search';}" onFocus="Javascript: this.value='';" VALUE="Search" class="hede" NAME="find"><INPUT TYPE="image" SRC="go_whiteontrans.png" align="middle" width="15" height="15" border="0" alt="Go!">
<div class="logo">
  <a class="hede" href="https://en.wikipedia.org/wiki/Wiki">What is a wiki?</a>
</FORM>
  
</div>
  </p>
  <p class="hedebottom">
  <A class="hede" href="/index.php?page=EtreeWiki">EtreeWiki</A> |
  <A class="hede" href="/index.php?page=RecentChanges">RecentChanges</A> |
  <A class="hede" href="/index.php?action=prefs">Preferences</A><br>
  <A class="hede" href="/index.php?page=EtreeWikiGoals">EtreeWikiGoals</A> |
  <A class="hede" href="/index.php?page=AboutThisSite">AboutThisSite</A> |
  <A class="hede" href="/index.php?page=EditingTips">EditingTips</A>
</p>
  </td>
  <td class="mid" valign="top" width="100" cellpadding="0" cellspacing="0" background="hede_mid.png" align="bottom">
  </td>
  <td class="right" valign="bottom" width="250" cellpadding="0" cellspacing="0" background="hede_right_wiki.png" onClick="window.location = '/';">&nbsp;
  </td>
 </tr>
</table>
<!-- END HEDE BANNER -->

<!-- BEGIN PAGE TITLE / WIKI SUBJECT -->
<a href="index.php?action=find&find=NamingStandards"><font class="wikititle">Naming Standards</font></a>
<hr>
<!-- END PAGE TITLE / WIKI SUBJECT -->
<!-- BODY CONTENT -->
<div class="right-pd">
<div id="body">
<h2>Etree.org Conventions for File Distributions</h2>For many reasons, files distributed via etree.org should follow the etree.org naming system.  When filesets are consistently named and organized, they are easier for downloaders to identify, catalog, archive and keep track of.  Badly named files are confusing when they do not identify the artist or track properly.  Worse, files with spaces or characters like ?s or 's do not always work on every operating system.  There are other reasons for using the etree.org naming standard, which has developed over time to benefit the entire community.  <br />
<br />
Please use this naming scheme along with our <a href="http://wiki.etree.org/index.php?page=SeedingGuidelines">SeedingGuidelines</a> when creating new filesets (aka "seeds"). The separate <a href="https://www.archive.org/details/etree">[Live Music Archive]</a> collection at the <a href="https://www.archive.org">[Internet Archive]</a> also asks for these naming standards for <a href="https://www.archive.org/about/faqs.php#119">[uploads]</a> of <a href="https://www.archive.org/about/faqs.php#100">[allowed material]</a> to their site.<br />
<br />
There are slight differences in naming between <a href="http://wiki.etree.org/index.php?page=Shorten">Shorten</a> and <a href="http://wiki.etree.org/index.php?page=FLAC">FLAC</a> sets. Both are covered below.<br />
<br />
<strong>New:  <a href="http://wiki.etree.org/index.php?page=ExtendedNamingStandards">ExtendedNamingStandards</a></strong> - Top directory names that hold even more information are quickly becoming a preferred convention for seeded shows. These standards are especially handy when multiple versions circulate for a date. Please read them after reading through this page.<br />
<br />
<br />
<h3>Basic Naming Scheme for Both Shorten and FLAC Sets</h3>Every file in a set (whether .shn, .md5, .txt, .flac, etc) and any directory for the set should have a name which starts like this: <br />
<br />
<dl><dd><tt>bbyyyy-mm-dd</tt></dd>
<dd><tt>^^^^^^ ^^ ^^</tt></dd>
<dd><tt>|||||| || \\___ Day (with leading zero)</tt></dd>
<dd><tt>|||||| ||</tt></dd>
<dd><tt>|||||| \\______ Month (with leading zero)</tt></dd>
<dd><tt>||||||</tt></dd>
<dd><tt>||\\\\_________ Year (Full 4 digit year)</tt></dd>
<dd><tt>||</tt></dd>
<dd><tt>\\___________ Band Abbreviation, lowercase preferred (e.g. ph = Phish, see <a href="http://wiki.etree.org/index.php?page=BandAbbreviations">BandAbbreviations</a> for examples)</tt></dd>
</dl>
<br />
Use only letters, numbers, hyphens (-), and when necessary, periods (.) or underscores (_). <strong>Do not</strong> include spaces, ampersands (&amp;), slashes or any other special characters. They cause problems in many cases. One <a href="https://www.archive.org/about/faqs.php#216">[list of disallowed characters]</a> can be found in the <a href="https://www.archive.org/details/etree">[Live Music Archive]</a> FAQ.<br />
<br />
Shorten and FLAC sets will differ in what comes after this part, as follows:<br />
<br />
<br />
<h3>a) <a href="http://wiki.etree.org/index.php?page=Shorten">Shorten</a> Distributions</h3>All files in a "shn set" must be enclosed within a directory (aka folder). This makes them easy to sort and transfer as a group. The directory name should follow the Basic Naming Scheme, but with a .shnf extension appended. The "f" is for "folder", to show that you're looking at a directory rather than a .shn filetype. (The difference would be obvious at a glance in Windows, but not necessarily in other operating systems like Unix.)<br />
<br />
Inside this directory, place the info file (.txt) for this seed, the .shn files each named by CD disc and track, and the .md5 checksums for the .shn files. See <a href="http://wiki.etree.org/index.php?page=SeedingGuidelines">SeedingGuidelines</a> for tips on writing a proper info file and generating .md5 files.<br />
<br />
Below is an example of what a shn set should look like: <br />
<br />
<dl><dd><tt>ph2000-04-20.shnf  &lt;--- Directory for the set- the f shows it's a "folder"</tt><dl><dd><tt>ph2000-04-20.md5 &lt;--- md5 checksum file for all discs together</tt></dd>
<dd><tt>ph2000-04-20.txt  &lt;--- Show info file</tt></dd>
<dd><tt>ph2000-04-20d1t01.shn \</tt></dd>
<dd><tt>ph2000-04-20d1t02.shn  \ __ disc one .shn files </tt></dd>
<dd><tt>ph2000-04-20d1t03.shn  /</tt></dd>
<dd><tt>ph2000-04-20d1t04.shn /</tt></dd>
<dd><tt>ph2000-04-20d2t01.shn \</tt></dd>
<dd><tt>ph2000-04-20d2t02.shn    \ __ disc two .shn files</tt></dd>
<dd><tt>ph2000-04-20d2t03.shn   / </tt></dd>
<dd><tt>ph2000-04-20d2t04.shn /</tt></dd>
<dd><tt>ph2000-04-20wav.md5 &lt;--- optional md5 checksum file for your original wavs</tt></dd>
</dl>
</dd>
</dl>
<br />
<strong>Optional:</strong> Instead of <br />
<dl><dd><dl><dd><tt>ph2000-04-20.md5 &lt;--- md5 checksum file for all discs together</tt></dd>
</dl>
</dd>
</dl>
you may use<br />
<dl><dd><dl><dd><tt>ph2000-04-20d1.md5 &lt;--- md5 checksum file for disc 1</tt></dd>
<dd><tt>ph2000-04-20d2.md5 &lt;--- md5 checksum file for disc 2</tt></dd>
</dl>
</dd>
</dl>
<br />
<strong>Important:</strong> Place a 0 before any single-digit track number (1 to 9). Without the 0, 10 would come before 2 during file sorting. This can mess up track orders when the decompressed files are burned to audio discs.<br />
<br />
<br />
<h3>b) <a href="http://wiki.etree.org/index.php?page=FLAC">FLAC</a> Distributions</h3>FLAC directory naming has an extra twist compared to Shorten. The FLAC codec supports both 16-bit and 24-bit content, in contrast to Shorten, which supports only 16-bit audio content. Because of these multiple bit-depths, we use the <tt><strong>.flac16</strong></tt> and <tt><strong>.flac24</strong></tt> naming convention to distinguish each type. <strong>Don't</strong> use <tt><strong>.flac</strong></tt> or <tt><strong>.flacf</strong></tt> when naming a directory. These distinction are important! Some reasons to care: <br />
<br />
<ul><li>Using <tt><strong>.flac</strong></tt> as a directory suffix will confuse the upload checking software at the <a href="https://www.archive.org/details/etree">[Live Music Archive]</a>, and may confuse regular users too. This extension makes the directory look like a file, instead of a whole directory.</li>
<li>Typical downloaders will want to watch for <tt><strong>.flac16</strong></tt> seeds. The 16-bit encoded FLAC files can be turned directly into traditional audio CDs, just like we do with Shorten files.</li>
<li>As the lossless audio community slowly adopts and purchases 24 bit capable gear (or FLAC-capable hardware decoders), the demand for higher-quality audio recording resolutions will increase.  In addition, users without 24 bit audio support will want to know to stay away from <tt><strong>.flac24</strong></tt> folders because they won't be able to take advantage of the additional audio clarity that 24 bit recordings offer.</li>
<li>Tapers and seeders who want to archive their original 24 bit content, in addition to the 16 bit audio that they seeded for wider distribution, can use the <tt><strong>.flac16</strong></tt> and <tt><strong>.flac24</strong></tt> folder extensions to archive files easily on larger storage media such as DVD-R/RW and DVD+R/RW.</li>
</ul>
<br />
All files in a "flac set" must be enclosed within a directory (aka folder). This makes them easy to sort and transfer as a group. The directory name should follow the Basic Naming Scheme. Inside the .flac16 or .flac24 directory, place the info file (.txt) for this seed, the .flac files (each named by CD disc and track if appropriate), and a file containing the <a href="http://wiki.etree.org/index.php?page=FlacFingerprint">FlacFingerprint</a> (ffp.txt) of every .flac file. See <a href="http://wiki.etree.org/index.php?page=SeedingGuidelines">SeedingGuidelines</a> for tips on writing a proper info file.<br />
<br />
Below is an example of what a 16-bit flac set should look like, tracked for audio CDs. It's possible 24-bit flac sets* might be laid out differently, given archiving needs. <br />
<br />
<dl><dd><tt>ph2003-04-20.flac16  &lt;--- Directory for the set</tt><dl><dd><tt>ph2003-04-20.txt  &lt;--- Show info file</tt></dd>
<dd><tt>ph2003-04-20ffp.txt &lt;--- <a href="http://wiki.etree.org/index.php?page=FlacFingerprint">FlacFingerprint</a> file for the wav portion of the .flac files</tt></dd>
<dd><tt>ph2003-04-20d1t01.flac \</tt></dd>
<dd><tt>ph2003-04-20d1t02.flac   \ __ disc one .flac files </tt></dd>
<dd><tt>ph2003-04-20d1t03.flac   / </tt></dd>
<dd><tt>ph2003-04-20d1t04.flac /</tt></dd>
<dd><tt>ph2003-04-20d2t01.flac \</tt></dd>
<dd><tt>ph2003-04-20d2t02.flac   \ __ disc two .flac files</tt></dd>
<dd><tt>ph2003-04-20d2t03.flac   / </tt></dd>
<dd><tt>ph2003-04-20d2t04.flac /</tt></dd>
<dd><tt>ph2000-04-20.md5 &lt;--- optional md5 whole-file checksum file for your .flac files</tt></dd>
</dl>
</dd>
</dl>
<br />
*Mike Wren writes: "We still need someone to write up a seeding standard for 24bit audio (how do we track, how do we archive for DVD-A support, etc.).  I'll be bugging people about this  --MW"<br />
<br />
<strong>Important:</strong> Place a 0 before any single-digit track number (1 to 9). Without the 0, 10 would come before 2 during file sorting. This can mess up track orders when the decompressed files are burned to audio discs.<br />
<br />
<br />
<h3>File Sorting Note</h3>Prior etree.org standards called for the use of second-level directories within filesets, such as <tt><strong>ph2000-04-20d1.shnf</strong></tt>, with each disc's worth sorted into an individual folder. We now feel that is unnecessary. As long as each file denotes disc and track, that should be good enough. <br />
<br />
Likewise, prior Shorten naming standards called for each disc's worth of shns to have its own .md5 file. However, if all .shn files are sorted into a single directory as suggested above, it is natural to generate a <em>single</em> .md5 file for the whole bunch.<br />
<br />
You will still see many disc-sorted filesets and checksums in circulation. <em><strong>Either way, sorted or together, is fine.</strong></em> There is <em>no</em> need to change old directory structures or .md5 file arrangements.<br />
<br />
<h3>Handling Mics Or Other Source Info</h3><br />
Often, it's convenient to include mic info or other important source information in the filename itself.  That way, two filesets from the same date, but from different sources, won't conflict with each other.  The best way to do this is to put the unique info after the date and track part of the filename, and before the extension.  For example, these all work:<br />
<dl><dd><tt>ph2003-03-01.dpa.shnf</tt></dd>
<dd><tt>ph2003-03-01-nak.shnf</tt></dd>
<dd><tt>ph2003-03-01cmc6.shnf</tt></dd>
</dl>
<strong>Do not</strong> use the "&amp;" character, because most operating systems <a href="https://www.archive.org/about/faqs.php#216">[won't accept it]</a> in a filename.<br />
<br />
Naturally, individual files would be named the same way -- all extra info goes after the date info and before the file extension, like this:<br />
<dl><dd><tt>gd1973-02-09d1t01bertha.shn</tt></dd>
<dd><tt>gd1973-02-09berthad1t01.shn</tt></dd>
</dl>
Either way works OK, although the first example is better than the second.  Done this way, files and directories will still sort correctly, two filesets from a single band and date won't conflict or overwrite each other, and users can tell the source at a glance.  Also, <a href="http://wiki.etree.org/index.php?page=FurthurNet">FurthurNet</a> will accept files shared this way.  It won't accept them if you put the source information anywhere else in the filename.<br />
<br />
<h3>Other Naming Resources</h3><br />
Related sites with sensible naming conventions for filesets:<br />
<ul><li><a href="http://www.furthurnet.org">[FurthurNet]</a>- As of version 1.7.2, uses an enforced <a href="http://forums.furthurnet.com/viewtopic.php?t=7994">[naming convention]</a> similar to above. Abbreviations they use can be found on their <a href="http://furthurnet.org/bandlist/">[Bandlist]</a>. </li>
<li><a href="https://www.archive.org/details/etree">[Live Music Archive]</a>:<ul><li>Naming conventions are enforced to exclude special characters, see <a href="https://www.archive.org/about/faqs.php#216">[FAQ item]</a> and <a href="https://www.archive.org/iathreads/post-view.php?id=8944">[1]</a>.</li>
<li>5/2006+, new upload system at LMA: In the first step of the "item creation" process for new-system uploads, you will be walked through creation of the "identifier" for the fileset. This will become the fileset's directory at the site, into which you will upload the files. The naming scheme for the identifier is based on these standards.</li>
<li>Normal <em>all-letter</em> band abbreviations should go right next to the year, <em>no</em> hyphen, like sciyyyy-mm-dd. In contrast, the few <em>number-containing</em> abbreviations (eg, sts9 or 311) can take a hyphen there for clarity, like sts9-yyyy-mm-dd.</li>
</ul>
</li>
</ul>
<br />
Some handy tools for renaming files are <a href="http://hp.vector.co.jp/authors/VA014830/english/FlexRena/">[Flexible Renamer]</a>, <a href="http://www.rlvision.com/flashren/about.asp">[Flash Renamer]</a>, <a href="http://www.herve-thouzard.com/modules/wfsection/article.php?articleid=1">[THE Rename]</a> (Windows free/shareware). Be careful not to rename all the files on your hard drive accidentally, though!<br />
<br />
<em>See also: <a href="http://wiki.etree.org/index.php?page=ExtendedNamingStandards">ExtendedNamingStandards</a>, <a href="http://wiki.etree.org/index.php?page=SeedingGuidelines">SeedingGuidelines</a>, <a href="http://wiki.etree.org/index.php?page=BandAbbreviations">BandAbbreviations</a>, <a href="http://wiki.etree.org/index.php?page=BandAbbreviationGuidelines">BandAbbreviationGuidelines</a>, <a href="http://wiki.etree.org/index.php?page=LiveMusicArchive">LiveMusicArchive</a>, <a href="http://wiki.etree.org/index.php?page=FurthurNet">FurthurNet</a></em><br />
<hr />
 <br />
Thanks to <a href="https://groups.yahoo.com/group/pcp/">[People for a Clearer Phish]</a> for developing the original standards.<br />
</div>

</div>
<!-- END BODY CONTENT -->

<!-- BEGIN WIKI DATA and CONTROLS (edit, view history) -->
<hr>
<font class="pagecontrol">This Page Last Changed: Nov 27, 2007 06:06:27</font><br>

	<A class="pagecontrol" href="/index.php?action=edit&page=NamingStandards">
		Edit this page
	</a>

	</A> &middot;
	<A class="pagecontrol" href="/index.php?action=history&page=NamingStandards">View page history</A><br>
<!-- END WIKI DATA and CONTROLS (edit, view history) -->

<!-- BEGIN COPYRIGHT FOOTER -->
<hr>
<div class="right-pd" align="right">
<font class="copyright" align="right">All content written by members of the etree.org community, for the etree.org community.<br>
&copy; 1998-2019, All Rights Reserved.  <!-- Check out our <A href="privacy.html">Privacy Policy</a> --></p>
<!-- END COPYRIGHT FOOTER -->


</BODY>
</HTML>