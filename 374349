<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Version Control with Subversion</title>
<link rel="stylesheet" href="./styles.css" type="text/css" />
</head>

<body>

<div id="main">
<div id="content">

<h1 class="bar">&nbsp;</h1>
<h1 class="tagline">The Standard in Open Source Version Control</h1>
<h1 class="title1">Version Control with</h1>
<h1 class="title2">Subversion</h1>

<!-- CONTENT STARTS -->

<h2>Introduction</h2>

<p>This is the home of <span class="underline">Version Control with
   Subversion</span>,
   a <a href="http://svnbook.red-bean.com/en/1.7/svn.copyright.html"
   >free</a> book about the ubiquitous
   <a href="http://subversion.apache.org/" >Apache&trade;
   Subversion&reg;</a> version control system and written by some of
   the developers of Subversion itself.</p>

<p>As you may have guessed from the layout of this page, we are quite
   pleased that some versions of this book have
   been <a href="http://www.oreilly.com/catalog/0596004486/"
   >published</a> by O'Reilly Media.  You can
   certainly <a href="/buy/">buy</a> a traditionally published print
   copy of the book if you'd like to, but you'll always find the most
   recent versions here on this site, available in both HTML and PDF
   formats.</p>

<h2>Online Versions of the Book</h2>

<p>Versions of this book use a numbering system designed to match
   those used by the Subversion software itself&mdash;version 1.7 of
   <span class="underline">Version Control with Subversion</span>
   covers Subversion 1.7, for example.  Here are the latest versions
   of the book which are available online:</p>

<div id="versionlist" class="versionlist">

<h3>For Subversion 1.7</h3>

<ul>
<li>View the <a href="./en/1.7/index.html">multiple-page HTML edition</a>
    of the book.
    [<a href="./en/1.7/svn-book-html-chunk.tar.bz2">tar.bz</a>]</li>
<li>View the <a href="./en/1.7/svn-book.html">single-page HTML edition</a>
    of the book.
    [<a href="./en/1.7/svn-book-html.tar.bz2">tar.bz</a>]</li>
<li>View the <a href="./en/1.7/svn-book.pdf">PDF edition</a> of the
    book.</li>
<li>View the book's
    <a href="https://sourceforge.net/p/svnbook/source/HEAD/tree/branches/1.7/en/book/"
    >DocBook XML sources</a>.</li>
</ul>

<h3>Nightly Build (for Subversion 1.8)</h3>

<p>Please bear in mind that these versions are works-in-progress: if
   you bookmark or link to specific sections, those links may be
   invalidated by continuing development. If you need a link that can
   be reasonably expected to remain stable for years to come, link to
   one of the completed editions above.</p>

<ul>
<li>View the <a href="./nightly/en/index.html">multiple-page HTML edition</a>
    of the book.
    [<a href="./nightly/en/svn-book-html-chunk.tar.bz2">tar.bz</a>]</li>
<li>View the book's <a
    href="https://sourceforge.net/p/svnbook/source/HEAD/tree/trunk/en/book/">DocBook 
    XML sources</a>.</li>
</ul>

</div>

<p>You can also find older versions of the book (which we suspect are
   no longer of much interest to most folks) <a href="old-versions.html" 
   >here</a>.</p>

<h2>Feedback/Contributing</h2>

<p>For feedback on the book or this website, contact
   <tt>&#115;&#118;&#110;&#98;&#111;&#111;&#107;&#45;&#100;&#101;&#118;&#64;&#114;&#101;&#100;&#45;&#98;&#101;&#97;&#110;&#46;&#99;&#111;&#109;</tt>
   [<a
   href="&#104;&#116;&#116;&#112;&#58;&#47;&#47;&#119;&#119;&#119;&#46;&#114;&#101;&#100;&#45;&#98;&#101;&#97;&#110;&#46;&#99;&#111;&#109;&#47;&#109;&#97;&#105;&#108;&#109;&#97;&#110;&#47;&#108;&#105;&#115;&#116;&#105;&#110;&#102;&#111;&#47;&#115;&#118;&#110;&#98;&#111;&#111;&#107;&#45;&#100;&#101;&#118;">listinfo</a>].
   If you have spotted errors in the book (O'Reilly's hardcopy or
   otherwise), please do the following things:</p>

<ol>
<li>Check our <a href="https://sourceforge.net/p/svnbook/tickets/"
    >issue/ticket tracker</a> to see if someone else has already reported the
    same problem.  If so, there's nothing else to do, unless you wish
    to contribute a patch which fixes the problem (see below).</li>
<li>Check the XML sources to see if the problem still exists.  You can
    grab these using Subversion itself, by checking out the trunk of our
    Subversion repository at
    <a href="https://svn.code.sf.net/p/svnbook/source/trunk/" 
    >https://svn.code.sf.net/p/svnbook/source/trunk/</a>.
    If the problem is present in the latest book sources, please
    report the problem to the mailing list above or file
    a <a href="https://sourceforge.net/p/svnbook/tickets/new/">new
    ticket</a>.</li>
<li>If the problem is in the published second edition book, check O'Reilly's
    <a href="http://www.oreilly.com/catalog/9780596510336/errata/">errata
    page</a> for the book, and report the error there if it hasn't
    already been reported.</li>
</ol>

<p>Reports of errors in the book are always welcome.  Reports of
   errors in the book which are accompanied by a suggested fix for the
   problem are even better.  For technical fixes (spelling, grammar,
   markup, etc.), just include with your error-reporting email a patch
   against the XML sources (and include the word <tt>[PATCH]</tt> in
   the subject line).  For more subjective concerns about the tone or
   comprehensibility of a passage, it's best just raise that topic on
   the mailing list.</p>

<h2>Translations</h2>

<p>This book has been (or is being) translated to other languages.
   Use the navigation menu at the bottom of the page to select a
   different language.  From each translated page you can get
   instructions on obtaining the translated book (or a
   work-in-progress snapshot if it is not finished yet).  Note that
   the English version is the master from which all translations
   derive, and if you have any comments about a translation you should
   contact that translation's authors.</p>

<!-- NON-ENGLISH TRANSLATION-SPECIFIC STUFF BEGINS -->

<!-- NON-ENGLISH TRANSLATION-SPECIFIC STUFF ENDS -->

<h2>Are Those Turtles?</h2>

<p>Why, yes, they are, indeed, turtles.  That's the animal chosen by
   the publisher for our book cover.  And before you ask us, "Why?"
   &mdash; we don't really know.  It's cool, and our wives are pleased
   that at least something "icky" wasn't chosen to represent
   Subversion.</p>

<!-- CONTENT ENDS -->

<h1 class="authorlist"><a href="http://www.red-bean.com/sussman/">Ben
   Collins-Sussman</a>,<br /><a
   href="http://www.red-bean.com/fitz/">Brian W. Fitzpatrick</a> &amp;
   <a href="http://www.red-bean.com/cmpilato/">C. Michael
   Pilato</a></h1>

<hr />

<p id="translinks">This page is also available in the following languages:<br />

<!-- For ease of maintenance, keep these sorted by language code.
     TRANSLATORS: titles here are in your native language; link text
     is in the language of the linked page. -->

   <a href="index.de.html" title="German" 
      hreflang="de" lang="de" rel="alternate">Deutsch</a> |
   <a href="index.es.html" title="Spanish" 
      hreflang="es" lang="es" rel="alternate">español</a> |
   <a href="index.fr.html" title="French" 
      hreflang="fr" lang="fr" rel="alternate">français</a> |
   <a href="index.it.html" title="Italian" 
      hreflang="it" lang="it" rel="alternate">Italiano</a> |
   <a href="index.ja.html" title="Japanese"
      hreflang="ja" lang="ja" rel="alternate">&#26085;&#26412;&#35486;</a> |
   <a href="index.nb.html" title="Norwegian" 
      hreflang="nb" lang="nb" rel="alternate">norsk</a> |
   <a href="index.pt_BR.html" title="Portuguese" 
      hreflang="pt-BR" lang="pt-BR" rel="alternate">Português</a> |
   <a href="index.ru.html" title="Russian"
      hreflang="ru" lang="ru" rel="alternate"
      >&#1056;&#1091;&#1089;&#1089;&#1082;&#1080;&#1081;</a> |
   <a href="index.vi.html" title="Vietnamese"
      hreflang="vi" lang="vi" rel="alternate">Tiếng Việt</a> |
   <a href="index.zh.html" title="Chinese" 
      hreflang="zh" lang="zh" rel="alternate">&#20013;&#25991;</a>
</p>

</div> <!-- #content -->
</div> <!-- #main -->

</body>
</html>
