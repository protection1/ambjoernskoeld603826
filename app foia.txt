<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head>
<title>Corporation for National Research Initiatives (CNRI)</title>

<meta name="description" content="Website for Corporation for National Research Initiatives"> 
<meta name="keywords" content="CNRI, Corporation for National Research Initiatives, Digital Object Architecture, Handle System, DO Registry, DO Repository">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.png" type="image/png">

<link href="style/cnri-style.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
// Copyright 2006-2007 javascript-array.com

var timeout	= 500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose; 
</script>

</head>

<body style="width: 840px;">

<div style="width:840px;height:1px;background:#75948a"></div>
<div style="width:840px;height:1px;background:#ffffff"></div>
<div style="width:840px;height:1px;background:#75948a"></div>

<!-- TABLE FOR BANNER IMAGE -->
<table width="840"><tbody><tr><td>
<img src="img/banner-513.gif" alt="Logo" width="620" height="112" title="Corporation for National Research Initiatives" align="left" border="0">
</td></tr></tbody></table>
<!-- END TABLE -->

<div style="width:840px;height:1px;background:#75948a"></div>
<div style="width:840px;height:1px;background:#ffffff"></div>
<div style="width:840px;height:1px;background:#75948a"></div>
<div style="width:840px;height:10px;background:#ffffff"></div>

<!-- TABLE FOR NAVIGATION BAR -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody><tr>    
<td height="36" width="30" bgcolor="#4a7205">&nbsp;</td> 
<td height="36" bgcolor="#4a7205">    
<ul id="sddm">
	<li><a href="index.html">HOME</a></li> 
    <li><a href="about_cnri.html" onmouseover="mopen('m1')" onmouseout="mclosetime()">ABOUT CNRI</a>
        <div id="m1" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
        <a href="about_cnri.html#officers">Directors, Officers &amp; Senior Staff</a>
        <a href="news.html">News  &amp; Announcements</a>
        <a href="jobs.html">Employment Opportunities</a>
        </div>
    </li>
    <li><a href="activities.html">RESEARCH FOCUS AREAS</a></li>    
    <li><a href="projects.html">PROJECTS</a></li>
    <li><a href="publications.html">PUBLICATIONS</a></li>
    <li><a href="contacts.html">CONTACTS</a></li>
</ul>
<div style="clear:both"></div>
</td>    
</tr></tbody></table>
<!-- END TABLE FOR NAVIGATION BAR -->


<div align="center">
<img src="img/home-2014.jpg" alt="Photograph of CNRI Building" align="middle" width="840" vspace="10" height="338">
</div>

<p>Corporation for National Research Initiatives (CNRI) is a not-for-profit organization formed in 1986 to undertake, foster, and promote research in the public interest. Activities center around strategic development of network-based information technologies, providing leadership and funding for information infrastructure research and development.</p>

 <p>CNRI engages in prototyping and system and technology demonstration projects in order to further the design and implementation of selected infrastructure components for new computing- and communications-based applications. Current research expands upon the core components of CNRI's Digital Object Architecture; <a href="http://www.handle.net/">Handle.Net Registry</a> for assigning, managing, and resolving persistent identifiers for digital objects and other Internet resources, and <a href="http://cordra.org">Cordra.org</a> which provides a mechanism for the creation of, and access to, digital objects as discrete data structures. Other areas of interest support ongoing research in digital libraries and other networked information technologies, including the <a href="https://www.mems-exchange.org" name="outreach">MEMS and Nanotechnology Exchange</a>.</p>
 
 <div style="width:840px;height:1px;background:#75948a"></div>
  <div style="width:840px;height:1px;background:#ffffff"></div>
   <div style="width:840px;height:1px;background:#75948a"></div>
   
  <p><i><b>Of Special Interest:</b></i></p>
  
<!-- <p><i>CNRI announces the release of <a href="https://www.handle.net/download_hnr.html">Handle.Net (v8) Software</a>.</i></p> -->

<p><i><a href="papers/Kahn_ACICIP_Comments_16Dec2016.pdf">Comments at the Meeting of the Advisory Committee on International Communications and Information Policy (ACICIP)</a>, U. S. Department of State, December 16, 2016, by Robert E. Kahn, President &amp; CEO, Corporation for National Research Initiatives.</i></p>

<p><i>CNRI Announces <a href="http://cordra.org">Cordra</a>: Techniques for Long-term and Storage-independent Management of Digital Information.</i></p>

<p><i><a href="https://www.youtube.com/watch?v=RpwFgREgJzY&amp;index=2&amp;list=PLpoIPNlF8P2N20xKbt0BVJwOo8nfRcpAl">ITU Interviews Dr. Robert E. Kahn</a>, Chairman, CEO and President, CNRI. YouTube, published November 17, 2014. Topics included the role of the Digital Object Architecture (DOA) in combating counterfeit ICT products and devices, and what governments and the corporate sector can do to help.</i></p>

  <p><i><a href="news.html#ITURec">ITU-T Recommendation</a>, "Framework for discovery of identity management information", approved, based largely on CNRI's Digital Object Architecture.</i></p>
  
 <p><i>Lyons, Patrice A., "<a href="papers/Info_Sec_and_Privacy_2014.pdf">Managing Information in Digital Form</a>", in Information Security &amp; Privacy News, Information Security Committee, ABA Section of Science &amp; Technology Law, Volume 5, Issue 1, Winter 2014. See also <a href="papers/PartIV_ContentIdentification.pdf">Exhibit A: Part IV — Content Identification</a>, contribution of P. A. Lyons, Committee 703—Spring Report (2003), American Bar Association.</i></p> 
 
<!--   <p><i>Dr. Robert E. Kahn received the first <a href="news-qeprize.html">Queen Elizabeth Prize for Engineering</a> at a ceremony at Buckingham Palace.</i></p> -->
   
    <div style="width:840px;height:1px;background:#75948a"></div>
  <div style="width:840px;height:1px;background:#ffffff"></div>
   <div style="width:840px;height:1px;background:#75948a"></div>


<p class="footer">Corporation for National Research Initiatives<br> 1895 Preston White Drive, Suite 100, Reston, Virginia 20191<br>
    301 East High Street, Charlottesville, Virginia 22902<br>
    <a href="tel:(703) 620-8990">(703) 620-8990</a><br>January 13, 2017</p>

<div style="width:840px;height:1px;background:#75948a"></div>
<div style="width:840px;height:1px;background:#ffffff"></div>
<div style="width:840px;height:1px;background:#75948a"></div>
    

</body></html>